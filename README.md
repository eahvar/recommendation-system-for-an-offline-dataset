# Recommendation System for an offline dataset

It can be used for testing and running our recommendation system with an offline  (course) forum dataset file.
 
It includes two main files: “readDataSet.js” file which reads dataset information and “RecSystem.js” which includes the recommendation system.
Here we put a simple example of the dataset to show  its compatible format with our code. Dataset should be in “allThreads” folder and includes a number of thread folders  (i.e., one folder for each thread; index by 0,1,2 etc). Inside each thread, there is a “posts” folder to keep posts information (i.e., each post is kept in one txt file). “users.csv” gives users information and total information about threads is stored in “thread.csv”. Inside each thread folder, “thread.csv” keep general posts information of that thread.

Based on the number of the threads, amount of variable “m” in ReadThread function and also “r” in ReadPost function should be set. As the code uses LDA algorithm, a Javascript version library of LDA should also be available.
 
