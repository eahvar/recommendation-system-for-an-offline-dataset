// reads dataset and save
const fs = require('fs');
var readline = require('readline');
var dataArray, dataArray1;
var Questions=[];
var Answers=[];
var BehaviorY=[];
var TotalQuestions=0;
var TotalAnswers=0;
var Users ={UserID:{}, userName:{}, type:{}, Posts:[], BehaviorY:[]};//type=staff or student
var u=[];
var hh=1;
var UserNumber;
const util = require('util');
fs.readFileAsync = util.promisify(fs.readFile);//promisify readfile for solving async problem
const { promisify } = require("util");
const readFileAsync = promisify(fs.readFile);
var LocalStorage = require('node-localstorage').LocalStorage;
localStorage = new LocalStorage('./scratch');

exports.hello = function() {
    return (CollectionCreater());
      //return information to the main Recommendation System part;
    }

async function CollectionCreater () {
console.log(0);
 await ReadUsersInformation();
console.log('Return from ReadUsersInformation');
await ReadThread('./allThreads/0/thread.csv');
console.log('Return from ReadThread');
await ReadPost('./allThreads/0/posts/1.txt');
  console.log('UserNumber=',UserNumber);
console.log('*************end reading DataSet*****************');
console.log('u[0]=',u[0].userName);
await writeThreadstofile1();
return u;
}

async function ReadUsersInformation()
{
    console.log('ReadUsersInformation');

   var  lineReader = ( readline.createInterface({
      input: fs.createReadStream('users.csv')
    }));
let gg=await CreateUserObjects(lineReader);
}


 function CreateUserObjects(lineReader){
    // var i=1;
    return new  Promise(function (resolve) {
        lineReader.on('line',   (line) =>  {
               
                dataArray = line.split(";");  
                     if (hh>=2)//in order to not consider the first row, we start from line 2;  u[0]=UserID 1
                         {
                              u[hh-2] = Object.create(Users);
                              u[hh-2].UserID=dataArray[0];
                              u[hh-2].userName=dataArray[1];
                              u[hh-2].type=dataArray[2];   //staf or student
                         }
                 hh=hh+1;
                 UserNumber=hh-2;
                 resolve(2);
          });//end lineReader.on     
       
   });//end promise
   
}//end aa



//ReadPost:
async function ReadPost( PostFilePath){
    //'./allThreads/0/posts/1.txt'
    console.log('ReadPost');
    //read thread file
    try {
    	for(var k1=0;k1< UserNumber;k1++)
        {Questions[k1]=0; Answers[k1]=0; BehaviorY[k1]=0;
        }
    for(var r=0;r<16;r++)//16 is thread number=number of folders in allthreads forlder
        {      
        var data12=localStorage.getItem('Threaddd'+r);
        var threadlines= data12.split('\n');
        console.log('threadlines=',threadlines);
        console.log('data12.length=',threadlines.length-1);
        //seperates lines and words of csv file
        for (var j=1; j<threadlines.length-1; j++)
        {
        var data120=data12.split(/\r\n|\n/);//seperatest lines
        var data1200=data120[j].split(";");  //seperates words of a line
        const data = await readFileAsync('./allThreads/'+r+'/posts/'+data1200[1]+'.txt', 'utf8');
      
          for(var k=0;k< UserNumber;k++)
          {
              
               if (u[k].UserID==data1200[2]){  
               u[k].Posts= u[k].Posts+data.toString();
               if(j==1){TotalQuestions=TotalQuestions+1;
               Questions[k]=Questions[k]+1;   console.log('TotalQuestions=',TotalQuestions, 'TotalQuestions for', k+1, '=',Questions[k]);
               }else{TotalAnswers=TotalAnswers+1;Answers[k]=Answers[k]+1;console.log('TotalAnswers=',TotalAnswers, 'TotalAnswers for', k+1, '=',Answers[k]);}
               }
           }//end for k
      
        }//end for j
     
        }//end for 46 threads
   //compute learner behavior
    for(var k1=0;k1< UserNumber;k1++)
    {BehaviorY[k1]=((Answers[k1]/TotalAnswers)-(Questions[k1]/TotalQuestions)+1)/2;
    u[k1].BehaviorY=((Answers[k1]/TotalAnswers)-(Questions[k1]/TotalQuestions)+1)/2;
    console.log('BehaviorY for', k1+1, '=',BehaviorY[k1], 'Answers[k1]=',Answers[k1], 'Questions[k1]=',Questions[k1]);
    }
   } //try continue
  catch (err) {
    console.log('Error', err);
   }//end try
   
        console.log('ReadPost-end');
    } //end function ReadText


//ReadThread
async function ReadThread(ThreadFilePath){
    console.log('ReadThread');
    var j=1;
    try {
            for(var m=0;m<16;m++)//46:thread number=number of folders in allthreads forlder
            {
                const text = await readFileAsync('./allThreads/'+m+'/thread.csv', 'utf8');
                localStorage.setItem('Threaddd'+m, text.toString());
               // console.log('./allThreads/'+m+'/thread.csv');
               // console.log(text);
            }
    } catch (err) {
        console.log('Error', err);
    }
  
        console.log('End-ReadThread');
    }//end ReadThread

function writeThreadstofile1( ) {
    var AllThreads;
    for(var m=0;m< 16;m++)//16:thread number=number of folders in allthreads forlder
    {
        var K=localStorage.getItem('Threaddd'+m);
        AllThreads=AllThreads+K;
    }
    fs.writeFile('AllThreads.csv', AllThreads, (err) => {
         
        // In case of a error throw err.
        if (err) throw err;
    })
    }